<?php 

// tests/Util/CalculatorTest.php
namespace App\Tests;

use App\Controller\ecommrse;
use PHPUnit\Framework\TestCase;

class ecommersetest extends TestCase
{
    public function testAdd()
    {
        $ecommerse = new ecommrse();
        $result = $ecommerse->save(30, 12);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(42, $result);
    }
}
